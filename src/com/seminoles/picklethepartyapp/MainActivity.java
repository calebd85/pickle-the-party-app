package com.seminoles.picklethepartyapp;

import org.apache.cordova.DroidGap;

import android.os.Bundle;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;

public class MainActivity extends DroidGap {
	   
	@Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        super.loadUrl("file:///android_asset/www/index.html");
    }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
}