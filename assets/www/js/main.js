/********************** Initial Load Functions **********************/
$().ready(function () {
	//connectToDatabase();
	setTimeout(function() {
       homePageLoads();
    }, 3000);
	loginPageLoads();
	registerPageLoads();
	mainPageLoads();
	newEventPageLoads();
	archiveFeedPageLoads();
	eventDetailsPageLoads();
	//likeFeedPageLoads();
	$(document).ajaxStart(function() {
		$.mobile.loading( 'show', {
			  text: "Getting Parties!",
			  textVisible: true,
			  textonly: false,
			  html: ""
	  });
	});
	$(document).ajaxStop(function() {
	    $.mobile.loading('hide');
	});
});


/********************** Change Page Functions **********************/
function goToMainPage() {
	$.mobile.changePage( "#mainPage", { transition: "slide", reverse: true });
}

function goToArchiveFeedPage() {
	$.mobile.changePage( "#archiveFeedPage", { transition: "slide" });
}

function goToNewEventPage() {
	$.mobile.changePage( "#newEventPage", { transition: "pop" });
}

function goToEventDetailsPage() {
	$.mobile.changePage( "#eventDetailsPage", { transition: "pop" });
}

function goToLikesFeedPage() {
}

function goToHomePage() {
	$.mobile.changePage( "#homePage", { transition: "pop"});
}



//document.addEventListener("backbutton", function() {
//    if ($.mobile.activePage.attr('id') == "homePage") {
//        navigator.app.exitApp();
//    } else {
//        navigator.app.backHistory();
//    }
//}, false);

/********************** Common Functions **********************/
jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}

/********************** Creating Objects **********************/
function User(objUser) {
	var User = {};
	$.each(objUser, function (index,loggedUser) {
		User.UserId = loggedUser.userID;
		User.Pickcon = loggedUser.pickcon;
		User.Username = loggedUser.username;
		User.Password = loggedUser.password;
		User.FirstName = loggedUser.firstname;
		User.LastName = loggedUser.lastname;
		User.Age = loggedUser.age;
		User.Phone = loggedUser.phone;
		User.Email = loggedUser.email;
	});
	return User;
}

/********************** Connect to Database **********************/
function connectToDatabase() {
	$.ajax({
	    type: "Post",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: {action: 'connect'},
	    crossDomain: true,
	    success: function (response,status) {
	    },
	    error: function (response) {
	         alert("Error establishing a connection.");
	    }
    });
}

/********************** Capture Photo Functions **********************/
function capturePhoto() {
	var pictureSource=navigator.camera.PictureSourceType;
	var destinationType=navigator.camera.DestinationType;
    navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 100,targetWidth: 200, targetHeight: 200,
        destinationType: destinationType.DATA_URI,
        correctOrientation: true});
}

function onPhotoDataSuccess(imageData) {
    var smallImage = document.getElementById('smallImage');
    smallImage.src = imageData;
}

function getPhoto() {
    // Retrieve image file location from specified source
	var pictureSource=navigator.camera.PictureSourceType;
	var destinationType=navigator.camera.DestinationType;
    navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 100, 
      destinationType: destinationType.FILE_URI,
      sourceType: pictureSource.PHOTOLIBRARY,
      correctOrientation: true});
    
      
  }

function onPhotoURISuccess(imageURI) {
    var largeImage = document.getElementById('smallImage');
    largeImage.src = imageURI;
}

function onFail(message) {
    alert('Failed because: ' + message);
}



/********************** Login/SignOut Functions **********************/
function homePageLoads(){
//forgot to commit and push
	if ($.mobile.activePage.attr('id') == 'splashPage') {
		if(localStorage.UserName){
			login(localStorage.UserName,localStorage.Password);
		}
		else
		{
			goToHomePage();
		}
	}
}


function signOut() {
	localStorage.removeItem('UserName');
	localStorage.removeItem('Password');
	$.mobile.changePage( "#homePage", { transition: "pop", changeHash: false });
}



/********************** Login Page Functions **********************/
function loginPageLoads() {
	$(".txtLogin").css({"background-color":"gray",
						"color":"white", "font-size":"20px",
						"border":"1px solid black"});
}

function prepareLogin() {
	login($('#logUsername').val(),$('#logPassword').val());
}

function login(username,password) {
	$.ajax({
	    type: "Post",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: {action: 'login', $username: username, $password: password },
	    crossDomain: true,
	    success: function (response,status) {
	    	if (response.toString().indexOf("match") >= 0) {
	    		getProfile(username,password);
	    	}
	    	else {
	    		alert('error');
	    	}
	    	
	    },
	    error: function (response) {
	         alert("Error establishing a connection.");
	    }
    });
}

function getProfile(username,password) {
	$.ajax({
	    type: "Post",
	    datatype: "json",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: {action: 'getProfile', $username: username },
	    crossDomain: true,
	    success: function (response,status) {
	    	var loggedUser = User(JSON.parse(response));
	    	$.data(document.body, "loggedUser", loggedUser);
	    	localStorage.UserName = username;
	    	localStorage.Password = password;
	    	$.mobile.changePage('#mainPage', {changeHash: false});
	    },
	    error: function (response) {
	         alert("Error establishing a connection.");
	    }
    });
}

/********************** Register Page Functions **********************/
// The fields are given onChange functions for quick validation
function registerPageLoads() {
	$(".txtLogin").css({"background-color":"gray",
		"color":"white", "font-size":"20px",
		"border":"1px solid black"});
	
	$("#txtUsername").change(function() {
		checkUsername($('#txtUsername').val());
	});
	
	$("#txtConfirm").change(function() {
		matchPasswords($('#txtPassword').val(), $('#txtConfirm').val());
	});
	
	$("#txtEmail").change(function() {
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var txtEmail = $('#txtEmail').val();
		if (!emailReg.test(txtEmail)) {
			$("#txtEmail").removeClass("passValidation").addClass("failValidation");
			var errorMessage = "That's not what an email looks like...";
			errorToast(errorMessage);
		}
		else {
			checkEmail(txtEmail);
		}
	});
}

// Call to database: Verify that the username is not already taken.
function checkUsername(username) {
	$.ajax({
	    type: "Post",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: {action: 'checkUsername', $username: username},
	    crossDomain: true,
	    success: function (response,status) {
	    	if (response.toString().indexOf("taken") >= 0) {
	    		$("#txtUsername").removeClass("passValidation").addClass("failValidation");
	    		var errorMessage = "Nah, you can't have that one.";
				errorToast(errorMessage);
	    	}
	    	else {
	    		$("#txtUsername").removeClass("failValidation").addClass("passValidation");
	    	}
	    },
	    error: function (response) {
	         alert("There was an issue validating the username.");

	    }
    });
}

// Validate the passwords and make sure that they match
function matchPasswords(original, confirmed){
	if (original == confirmed){
		$("#txtPassword").removeClass("failValidation").addClass("passValidation");
		$("#txtConfirm").removeClass("failValidation").addClass("passValidation");
	}
	else {
		$("#txtPassword").removeClass("passValidation").addClass("failValidation");
		$("#txtConfirm").removeClass("passValidation").addClass("failValidation");
		var errorMessage = "I see you're having trouble matching there..";
		errorToast(errorMessage);
	}
}

//Call to database: Verify that the email is not already taken.
function checkEmail(email) {
	$.ajax({
	    type: "Post",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: {action: 'checkEmail', $email: email},
	    crossDomain: true,
	    success: function (response,status) {
	    	if (response.toString().indexOf("taken") >= 0) {
	    		$("#txtEmail").removeClass("passValidation").addClass("failValidation");
	    		var errorMessage = "Nah, you can't have that email.";
	    		errorToast(errorMessage);
	    	}
	    	else {
	    		$("#txtEmail").removeClass("failValidation").addClass("passValidation");
	    	}
	    },
	    error: function (response) {
	         alert("There was an issue validating the email.");
	    }
    });
}

// If this returns true, all validation has passed.
function validateRegistration() {
	var validation = true;
	$('#registerPage .inputText').each(function () {
		if($(this).val() == "") {
			$(this).removeClass('passValidation').addClass('failValidation');
		}
	});
	
	$('.failValidation').each(function () {
		validation = false;
	});
	
	if (!validation) {
		var errorMessage = "You should fix the red things...";
		errorToast(errorMessage);
	}
	return validation;
}

function errorToast(error) {
	
	$('#registerPage .errorPopup').center();
	$('#registerPage .errorPopup').text(error);
	$('#registerPage .errorPopup').fadeIn(500);
	setTimeout(function() {
		$('.errorPopup').fadeOut(2000);
	}, 2000);
}


// Call to database: If all validation has passed, register the user.
function register() {
	if(validateRegistration()){
		$.ajax({
		    type: "Post",
		    url: "http://www.tremainegrant.com/Pickle/main.php",
		    data: {action: 'register', $username: $("#txtUsername").val(), $password: $("#txtPassword").val(), 
		    	   $email: $("#txtEmail").val()},
		    crossDomain: true,
		    success: function (response,status) {
		    	login($("#txtUsername").val(), $("#txtPassword").val());	
		    },
		    error: function (response) {
		         alert("There was an issue registering you.");
		    }
	    });
	}
}


/********************** Main Page Functions **********************/
function mainPageLoads() {
	$(document).delegate('#mainPage', 'pageshow', function () {
		$('#lblUserId').text($('body').data('loggedUser').UserId);
		$('#smallImage').attr('src', $('body').data('loggedUser').Pickcon);
	    $('#lblUsername').text($('body').data('loggedUser').Username);
	    $('#lblFirstName').text($('body').data('loggedUser').FirstName);
	    $('#lblLastName').text($('body').data('loggedUser').LastName);
	    $('#lblEmail').text($('body').data('loggedUser').Email);
	    $('#lblPicks').text('Picks 10');
	    $('#lblScore').text('Score 20');
	    getCurrentLocation();
	    //alert($('body').data('loggedUser').Pickcon);
	});
	
	$( ".photopopup" ).on({
        popupbeforeposition: function() {
            var maxHeight = $( window ).height() - 60 + "px";
            $( ".photopopup img" ).css( "max-height", maxHeight );
        }
    });
}

function getCurrentLocation () {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(retrievePosition);
    }
    else {
    	alert("Geolocation is not supported by this browser.");
    }
}

function retrievePosition(position) {
	var geocoder = new google.maps.Geocoder();
	if ($.mobile.activePage.attr('id') == 'mainPage') {
		retrieveMainFeed(position.coords.latitude, position.coords.longitude);
	}
	else if ($.mobile.activePage.attr('id') == 'archiveFeedPage') {
		retrieveMyEventFeed(position.coords.latitude, position.coords.longitude);
	}
	else if ($.mobile.activePage.attr('id') == 'eventDetailsPage') {
		retrieveMapLocation(position.coords.latitude, position.coords.longitude);
	}
	var yourLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	geocoder.geocode({ 'latLng': yourLocation }, function (results, status) {
		if(status == google.maps.GeocoderStatus.OK) {
			for (var i = 0; i < results[0].address_components.length; i++) { 
	        	if (results[0].address_components[i].types == "locality,political") {
	        		var city = results[0].address_components[i].long_name; 
	        		$('#cityHeadline').text(city);
	        	}
	    	}
		}
	});
}


function retrieveMainFeed(lat,lng) {
	$.ajax({
	    type: "Post",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: { action: 'mainFeed', $centerlat: lat, $centerlng: lng, $userID: $('body').data('loggedUser').UserId },
	    crossDomain: true,
	    success: function (response,status) {
	    	//alert(response);
	    	addEventFeed(JSON.parse(response));
	    },
	    error: function (response) {
	         alert("There was an issue retrieving this feed.");
	    }
    });
}

function addEventFeed(closeEvents) {
	$('#mainPage .eventCard').remove();
	$.each(closeEvents, function(index, closeEvent){
		switch (closeEvent.myeventstatus) {
		case 'H':
			$('#eventCards').append('<div class="eventCard"><table class="userDetails"><tr>' + 
					'<td style="width:40px;"><img src="./images/default.png" width="30" height="30" /></td>' +
					'<td><label id="mainFeedUsername">' + closeEvent.username + '</label></td>' + 
					'<td style="text-align:right;"><span class="ui-icon-location ui-btn-icon-right" style="position:relative; color:silver;">' + Math.ceil(closeEvent.eventdistance) + ' mi</span></td>' + 
					'</tr></table><div onclick="retrieveEventDetails('+ closeEvent.eventID +')" style="padding:5px; text-align:center; border: 1px solid silver; margin-bottom:5px;">' + closeEvent.eventname + '</div>' + 
					'<div style="text-align:center"><img src="./images/club_party.jpg" class="imgFlyer" /></div>' +
					'<table class="eventDetails" width="100%">' + 
					'<tr style="font-size:smaller; white-space: nowrap;"><td style="border:1px solid silver; padding:5px;">' + closeEvent.eventtype + '</td>' + 
					'<td style="border:1px solid silver; padding:5px;">' + closeEvent.eventmusic + '</td>'+ 
					'<td style="color:silver; text-align:right; width:100%">' + eventDateFormat(closeEvent.eventdate) + '</td></tr></table><table class="eventDetails" width="100%">' + 
					'<tr style="white-space: nowrap;"><td style="border:1px solid silver; padding:7px 0px 7px 0px; width:100%;max-width:100%;"><span class="ui-icon-plus ui-btn-icon-left" style="position:relative; color:silver;">Hosting</span></td>' + 
					'</table>'+'</div>');
			break;
		case 'A':
			$('#eventCards').append('<div class="eventCard"><table class="userDetails"><tr>' + 
					'<td style="width:40px;"><img src="./images/default.png" width="30" height="30" /></td>' +
					'<td><label id="mainFeedUsername">' + closeEvent.username + '</label></td>' + 
					'<td style="text-align:right;"><span class="ui-icon-location ui-btn-icon-right" style="position:relative; color:silver;">' + Math.ceil(closeEvent.eventdistance) + ' mi</span></td>' + 
					'</tr></table><div onclick="retrieveEventDetails('+ closeEvent.eventID +')" style="padding:5px; text-align:center; border: 1px solid silver; margin-bottom:5px;">' + closeEvent.eventname + '</div>' + 
					'<div style="text-align:center"><img src="./images/club_party.jpg" class="imgFlyer" /></div>' +
					'<table class="eventDetails" width="100%">' + 
					'<tr style="font-size:smaller; white-space: nowrap;"><td style="border:1px solid silver; padding:5px;">' + closeEvent.eventtype + '</td>' + 
					'<td style="border:1px solid silver; padding:5px;">' + closeEvent.eventmusic + '</td>'+ 
					'<td style="color:silver; text-align:right; width:100%">' + eventDateFormat(closeEvent.eventdate) + '</td></tr></table><table class="eventDetails" width="100%">' + 
					'<tr style="white-space: nowrap;"><td onclick="attendEvent(this,'+ closeEvent.eventID +');" style="border:1px solid silver; padding:7px 0px 7px 0px; width:50%;max-width:50%;"><span class="ui-icon-plus ui-btn-icon-left" style="position:relative; color:white;">Attend</span></td>' + 
					'<td onclick="followEvent(this,'+ closeEvent.eventID +');" style="border:1px solid silver; padding: 7px 0px 7px 0px;"><span class="ui-icon-eye ui-btn-icon-left" style="position:relative; color:silver;">Follow</span></td></table>'+
					'</div>');
			break;
		case 'F' :
			$('#eventCards').append('<div class="eventCard"><table class="userDetails"><tr>' + 
					'<td style="width:40px;"><img src="./images/default.png" width="30" height="30" /></td>' +
					'<td><label id="mainFeedUsername">' + closeEvent.username + '</label></td>' + 
					'<td style="text-align:right;"><span class="ui-icon-location ui-btn-icon-right" style="position:relative; color:silver;">' + Math.ceil(closeEvent.eventdistance) + ' mi</span></td>' + 
					'</tr></table><div onclick="retrieveEventDetails('+ closeEvent.eventID +')" style="padding:5px; text-align:center; border: 1px solid silver; margin-bottom:5px;">' + closeEvent.eventname + '</div>' + 
					'<div style="text-align:center"><img src="./images/club_party.jpg" class="imgFlyer" /></div>' +
					'<table class="eventDetails" width="100%">' + 
					'<tr style="font-size:smaller; white-space: nowrap;"><td style="border:1px solid silver; padding:5px;">' + closeEvent.eventtype + '</td>' + 
					'<td style="border:1px solid silver; padding:5px;">' + closeEvent.eventmusic + '</td>'+ 
					'<td style="color:silver; text-align:right; width:100%">' + eventDateFormat(closeEvent.eventdate) + '</td></tr></table><table class="eventDetails" width="100%">' + 
					'<tr style="white-space: nowrap;"><td onclick="attendEvent(this,'+ closeEvent.eventID +');" style="border:1px solid silver; padding:7px 0px 7px 0px; width:50%;max-width:50%;"><span class="ui-icon-plus ui-btn-icon-left" style="position:relative; color:silver;">Attend</span></td>' + 
					'<td class="following" onclick="followEvent(this,'+ closeEvent.eventID +');" style="border:1px solid silver; padding: 7px 0px 7px 0px;"><span class="ui-icon-eye ui-btn-icon-left" style="position:relative; color:white;">Following</span></td></table>'+
					'</div>');
			break;
		
		default:
			$('#eventCards').append('<div class="eventCard"><table class="userDetails"><tr>' + 
					'<td style="width:40px;"><img src="./images/default.png" width="30" height="30" /></td>' +
					'<td><label id="mainFeedUsername">' + closeEvent.username + '</label></td>' + 
					'<td style="text-align:right;"><span class="ui-icon-location ui-btn-icon-right" style="position:relative; color:silver;">' + Math.ceil(closeEvent.eventdistance) + ' mi</span></td>' + 
					'</tr></table><div onclick="retrieveEventDetails('+ closeEvent.eventID +')" style="padding:5px; text-align:center; border: 1px solid silver; margin-bottom:5px;">' + closeEvent.eventname + '</div>' + 
					'<div style="text-align:center"><img src="./images/club_party.jpg" class="imgFlyer" /></div>' +
					'<table class="eventDetails" width="100%">' + 
					'<tr style="font-size:smaller; white-space: nowrap;"><td style="border:1px solid silver; padding:5px;">' + closeEvent.eventtype + '</td>' + 
					'<td style="border:1px solid silver; padding:5px;">' + closeEvent.eventmusic + '</td>'+ 
					'<td style="color:silver; text-align:right; width:100%">' + eventDateFormat(closeEvent.eventdate) + '</td></tr></table><table class="eventDetails" width="100%">' + 
					'<tr style="white-space: nowrap;"><td class="Attending" onclick="attendEvent(this,'+ closeEvent.eventID +');" style="border:1px solid silver; padding:7px 0px 7px 0px; width:50%;max-width:50%;"><span class="ui-icon-plus ui-btn-icon-left" style="position:relative; color:silver;">Attend</span></td>' + 
					'<td onclick="followEvent(this,'+ closeEvent.eventID +');" style="border:1px solid silver; padding: 7px 0px 7px 0px;"><span class="ui-icon-eye ui-btn-icon-left" style="position:relative; color:silver;">Follow</span></td></table>'+
					'</div>');
		}
	});
}

function followEvent(item,eventID) {
	$.ajax({
	    type: "Post",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: { action: 'myEvent', $userID: $('body').data('loggedUser').UserId, $eventID: eventID, $myeventstatus: 'F' },
	    crossDomain: true,
	    success: function (response,status) {
	    	alert(response);
	    	var followSpan = $(item).find('span');
	    	followSpan.text('Following');
	    	followSpan.css('color','white');
	    	$(item).addClass('following');
	    },
	    error: function (response) {
	         alert("There was an issue when you tried to follow.");
	    }
    });
}

function attendEvent(item, eventID) {
	$.ajax({
	    type: "Post",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: { action: 'myEvent', $userID: $('body').data('loggedUser').UserId, $eventID: eventID, $myeventstatus: 'A' },
	    crossDomain: true,
	    success: function (response,status) {
	    	alert(response);
	    	var attendSpan = $(item).find('span');
	    	attendSpan.text('Attending');
	    	attendSpan.css('color','white');
	    	$(item).addClass('attending');
	    },
	    error: function (response) {
	         alert("There was an issue when you tried to attend.");
	    }
    });
}

function eventDateFormat(eventdate) {
	var d = new Date(eventdate);
	var short_MonthNames = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	var m_DayNames = new Array("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");
	var curr_date = d.getDate();
    var curr_month = d.getMonth();
    var weekday = d.getDay();
    var curr_hour = d.getHours();
    var curr_min = d.getMinutes();
    
    if (curr_min < 10) {
        curr_min = '0' + curr_min.toString();
    }
   
    return m_DayNames[weekday - 1] + ", " + short_MonthNames[curr_month] + "  " + curr_date + " at " + curr_hour + ":" + curr_min;
}

/********************** Archive Page Functions **********************/
function archiveFeedPageLoads() {
	$(document).delegate('#archiveFeedPage', 'pageshow', function () {
	    getCurrentLocation();
	    headerClickEvents();
	});
}

function headerClickEvents() {
	$('#btnHosting').click(function() {
		$(this).addClass('myEventHeaderSelected');
		$(this).css('border-bottom','2px solid green');
		$('#myHostedEventCards').show();
		
		$('#btnFollowing').removeClass('myEventHeaderSelected');
		$('#btnFollowing').css('border-bottom','1px solid silver');
		$('#myFollowedEventCards').hide();
		
		$('#btnAttending').removeClass('myEventHeaderSelected');
		$('#btnAttending').css('border-bottom','1px solid silver');
		$('#myAttendedEventCards').hide();
		
	});
	
	$('#btnFollowing').click(function() {
		$('#btnHosting').removeClass('myEventHeaderSelected');
		$('#btnHosting').css('border-bottom','1px solid silver');
		$('#myHostedEventCards').hide();
		
		$(this).addClass('myEventHeaderSelected');
		$(this).css('border-bottom','2px solid green');
		$('#myFollowedEventCards').show();
		
		$('#btnAttending').removeClass('myEventHeaderSelected');
		$('#btnAttending').css('border-bottom','1px solid silver');
		$('#myAttendedEventCards').hide();
	});
	
	$('#btnAttending').click(function() {
		$('#btnHosting').removeClass('myEventHeaderSelected');
		$('#btnHosting').css('border-bottom','1px solid silver');
		$('#myHostedEventCards').hide();
		
		$('#btnFollowing').removeClass('myEventHeaderSelected');
		$('#btnFollowing').css('border-bottom','1px solid silver');
		$('#myFollowedEventCards').hide();
		
		$(this).addClass('myEventHeaderSelected');
		$(this).css('border-bottom','2px solid green');
		$('#myAttendedEventCards').show();
	});
	
}

function retrieveMyEventFeed(lat,lng) {
	$.ajax({
	    type: "Post",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: { action: 'myEventFeed', $centerlat: lat, $centerlng: lng, $userID: $('body').data('loggedUser').UserId },
	    crossDomain: true,
	    success: function (response,status) {
	    	if(response){
	    		addMyEventFeed(JSON.parse(response));
	    	}
	    	else
	    		alert('no events');
	    },
	    error: function (response) {
	         alert("There was an issue retrieving this feed.");
	    }
    });
}

function addMyEventFeed(closeEvents) {
	$('#archiveFeedPage .eventCard').remove();
	$.each(closeEvents, function(index, closeEvent){
		switch (closeEvent.myeventstatus) {
		case 'H':
			$('#myHostedEventCards').append('<div class="eventCard"><table class="userDetails"><tr>' + 
				'<td style="width:40px;"><img src="./images/default.png" width="30" height="30" /></td>' +
				'<td><label id="mainFeedUsername">' + closeEvent.username + '</label></td>' + 
				'<td style="text-align:right;"><span class="ui-icon-location ui-btn-icon-right" style="position:relative; color:silver;">' + Math.ceil(closeEvent.eventdistance) + ' mi</span></td>' + 
				'</tr></table><div style="padding:5px; text-align:center; border: 1px solid silver; margin-bottom:5px;">' + closeEvent.eventname + '</div>' + 
				'<div style="text-align:center"><img src="./images/club_party.jpg" class="imgFlyer" /></div>' +
				'<table class="eventDetails" width="100%"><tr style="font-size:smaller; white-space: nowrap;"><td style="border:1px solid silver; padding:5px;">' + closeEvent.eventtype + '</td>' +
				'<td style="border:1px solid silver; padding:5px;">' + closeEvent.eventmusic + '</td>'+ 
				'<td style="color:silver; text-align:right; width:100%">' + eventDateFormat(closeEvent.eventdate) + '</td></tr></table>' + 
				'</div>');
			break;
		case 'F':
			$('#myFollowedEventCards').append('<div class="eventCard"><table class="userDetails"><tr>' + 
				'<td style="width:40px;"><img src="./images/default.png" width="30" height="30" /></td>' +
				'<td><label id="mainFeedUsername">' +  closeEvent.username + '</label></td>' + 
				'<td style="text-align:right;"><span class="ui-icon-location ui-btn-icon-right" style="position:relative; color:silver;">' + Math.ceil(closeEvent.eventdistance) + ' mi</span></td>' + 
				'</tr></table><div style="padding:5px; text-align:center; border: 1px solid silver; margin-bottom:5px;">' + closeEvent.eventname + '</div>' + 
				'<div style="text-align:center"><img src="./images/club_party.jpg" class="imgFlyer" /></div>' +
				'<table class="eventDetails" width="100%"><tr style="font-size:smaller; white-space: nowrap;"><td style="border:1px solid silver; padding:5px;">' + closeEvent.eventtype + '</td>' +
				'<td style="border:1px solid silver; padding:5px;">' + closeEvent.eventmusic + '</td>'+ 
				'<td style="color:silver; text-align:right; width:100%">' + eventDateFormat(closeEvent.eventdate) + '</td></tr></table>' + 
				'</div>');
			break;
		case 'A':
			$('#myAttendedEventCards').append('<div class="eventCard"><table class="userDetails"><tr>' + 
				'<td style="width:40px;"><img src="./images/default.png" width="30" height="30" /></td>' +
				'<td><label id="mainFeedUsername">' +  closeEvent.username + '</label></td>' + 
				'<td style="text-align:right;"><span class="ui-icon-location ui-btn-icon-right" style="position:relative; color:silver;">' + Math.ceil(closeEvent.eventdistance) + ' mi</span></td>' + 
				'</tr></table><div style="padding:5px; text-align:center; border: 1px solid silver; margin-bottom:5px;">' + closeEvent.eventname + '</div>' + 
				'<div style="text-align:center"><img src="./images/club_party.jpg" class="imgFlyer" /></div>' +
				'<table class="eventDetails" width="100%"><tr style="font-size:smaller; white-space: nowrap;"><td style="border:1px solid silver; padding:5px;">' + closeEvent.eventtype + '</td>' +
				'<td style="border:1px solid silver; padding:5px;">' + closeEvent.eventmusic + '</td>'+ 
				'<td style="color:silver; text-align:right; width:100%">' + eventDateFormat(closeEvent.eventdate) + '</td></tr></table>' + 
				'</div>');
			break;
		}
	});
}

/********************** New Event Page Functions **********************/
function newEventPageLoads() {
	$(document).delegate('#newEventPage', 'pageshow', function () {
		formChangeFunctions();
		addAutoComplete();
	});

}

function formChangeFunctions() {
	
	// Validate the Title once it has been entered
	$("#txtEventTitle").change(function() {
		var txtTooShort = 5;
		var txtTooLong = 20;
		if ($("#txtEventTitle").val().length < txtTooShort) {
			$("#txtEventTitle").removeClass("passValidation").addClass("failValidation");
			var errorMessage = "That title might be a little too short...";
			errorEventToast(errorMessage);
		}
		else if ($("#txtEventTitle").val().length > txtTooLong) {
			$("#txtEventTitle").removeClass("passValidation").addClass("failValidation");
			var errorMessage = "That title is kinda long...";
			errorEventToast(errorMessage);
		}
		else {
			$("#txtEventTitle").removeClass("failValidation").addClass("passValidation");
		}
	});
	
	$('#txtEventDescription').change(function() {
		if ($("#txtEventDescription").val() == "") {
			$("#txtEventDescription").removeClass("passValidation").addClass("failValidation");
			var errorMessage = "C'mon, let's be descriptive here!";
			errorEventToast(errorMessage);
		}
		else {
			$("#txtEventDescription").removeClass("failValidation").addClass("passValidation");
		}
	});
	
	// Validation and functionality of datetimepicker
	$('#datetimepicker').datetimepicker({
		format:'m/d/y @ g:iA',
		formatTime:'g:iA',
		onChangeDateTime:function(dp,$input){
			localStorage.EventDate = $input.val();
			var compareDate = changeDateFormat($input.val());
			var actualDate = new Date(compareDate);
			var currentDate = new Date(localStorage.currentDate);
			var convertActualDate = actualDate.getFullYear() + actualDate.getMonth() + 1 + actualDate.getDate();
			var convertCurrentDate = currentDate.getFullYear() + currentDate.getMonth() + 1 + currentDate.getDate();
			if (convertActualDate < convertCurrentDate){
				$("#datetimepicker").removeClass("passValidation").addClass("failValidation");
				var errorMessage = "The date must be in the future!";
				errorEventToast(errorMessage);
			}
			else {
				$("#datetimepicker").removeClass("failValidation").addClass("passValidation");
			}
		}
	});
	
	$('#datetimepicker').click(function () {
		$(this).blur();
	});
	
	// Validate that the user has chosen an address
	$('#txtEventStreet').change(function () {
		setTimeout(addressSelected,500);
	});
	
	$('#txtEventPrice').change(function() {
		var priceReg = /^[1-9]\d*(\.\d+)?$/;
		if (!priceReg.test($("#txtEventPrice").val())) {
			$("#txtEventPrice").removeClass("passValidation").addClass("failValidation");
			var errorMessage = "The price you enter should probably be a number.";
			errorEventToast(errorMessage);
		}
		else {
			$("#txtEventPrice").removeClass("failValidation").addClass("passValidation");
		}
	});
	
	// Add field to enter DJs username
	$('#chkEventDj').click(function() {
		$('#txtEventDj').toggle();
	});
}

function addressSelected() {
	if ($('#txtEventCity').val() == ''){
		$("#txtEventStreet").removeClass("passValidation").addClass("failValidation");
		var errorMessage = "Select one of the addresses, its easier that way.";
		errorEventToast(errorMessage);
	}
	else {
		$("#txtEventStreet").removeClass("failValidation").addClass("passValidation");
	}
}

function errorEventToast(error) {
	$('#newEventPage .errorPopup').center();
	$('#newEventPage .errorPopup').text(error);
	$('#newEventPage .errorPopup').fadeIn(500);
	setTimeout(function() {
		$('.errorPopup').fadeOut(2000);
	}, 2000);
}

function validateNewEvent() {
	var validation = true;
	$('#newEventPage .inputText').each(function () {
		if($(this).val() == "") {
			$(this).removeClass('passValidation').addClass('failValidation');
		}
	});
	
	$('#newEventPage select').each(function() {
		var selection = '#' + $(this).attr('id');
		if ($(selection + " option:selected").val() == '0') {
			var errorMessage = "Make sure you don't miss any steps!";
			errorEventToast(errorMessage);
			validation = false;
			return validation;
		}
	});

	$('.failValidation').each(function () {
		validation = false;
	});
	
	if (!validation) {
		var errorMessage = "You should fix the red things...";
		errorEventToast(errorMessage);
	}
	return validation;
}

function makeAnEvent() {
	if(validateNewEvent()) {
		$.ajax({
		    type: "Post",
		    url: "http://www.tremainegrant.com/Pickle/main.php",
		    data: {action: 'createEvent', $eventname: $("#txtEventTitle").val(), $eventdescription: $("#txtEventDescription").val(), 
		    	   $eventstreet: localStorage.Address, $eventcity: $("#txtEventCity").val(), $eventstate: $("#txtEventState").val(),
		    	   $eventzip: $("#txtEventZipCode").val(), $eventprice: $("#txtEventPrice").val(), $eventalch: $("#chkEventAlcohol").is(':checked'),
		    	   $eventprivacy: $("#chkEventPrivacy").is(':checked'), $eventdj: $("#chkEventDj").is(':checked'), $eventtype: $("#selEventType option:selected").val(),
		    	   $eventattire: $("#selEventAttire option:selected").val(), $eventmusic: $("#selEventMusic option:selected").val(), $userID: $('body').data('loggedUser').UserId, 
		    	   $eventdate: changeDateFormat(localStorage.EventDate), $createddate: localStorage.currentDate},
		    crossDomain: true,
		    success: function (response,status) {
		    		localStorage.removeItem('currentDate');
		    		localStorage.removeItem('EventDate');
		    		localStorage.removeItem('Address');
		    		goToMainPage();
		    },
		    error: function (response) {
		         alert("There was an issue creating this event.");
		    }
	    });
	}
}
	
function addAutoComplete() {
	var autocomplete = new google.maps.places.Autocomplete($("#txtEventStreet")[0], {});
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        for (var i = 0; i < place.address_components.length; i++) { 
        	if (place.address_components[i].types == "street_number") {
        		var street_number = place.address_components[i].long_name; 
        	}
        	else if (place.address_components[i].types == "route") {
        		var route = place.address_components[i].long_name; 
        	}
        	else if (place.address_components[i].types == "locality,political") {
        		var city = place.address_components[i].long_name; 
        	}
        	else if (place.address_components[i].types == "administrative_area_level_1,political") {
        		var state = place.address_components[i].short_name; 
        	}
        	else if (place.address_components[i].types == "postal_code") {
        		var zip_code = place.address_components[i].long_name; 
        	}
    	}
        localStorage.Address = street_number + ' ' + route;
        $('#txtEventCity').val(city);
		$('#txtEventState').val(state);
		$('#txtEventZipCode').val(zip_code);
    });
}

function changeDateFormat(eventDate) {
	var formattedDate = eventDate.replace(/\@/g, '');
    formattedDate = formattedDate.replace(/\P/g, '');
	formattedDate = formattedDate.replace(/\A/g, '');
	formattedDate = formattedDate.replace(/\M/g, '');
	var correctDate = new Date(formattedDate);
	var currentDate = new Date();
	localStorage.currentDate = currentDate;
	return correctDate.toString();
}

/********************** Event Details Page Functions **********************/
function eventDetailsPageLoads() {
	$(document).delegate('#eventDetailsPage', 'pageshow', function () {
	});
}

function retrieveEventDetails(eventID) {
	$.ajax({
	    type: "Post",
	    url: "http://www.tremainegrant.com/Pickle/main.php",
	    data: { action: 'getEventDetails', $eventID: eventID},
	    crossDomain: true,
	    success: function (response,status) {
	    	goToEventDetailsPage();
	    	displayEventDetails(JSON.parse(response));
	    },
	    error: function (response) {
	         alert("There was an issue when getting the event's details.");
	    }
    });
}

function displayEventDetails(closeEvents) {
	//$('#eventDetailsPage .eventCard').remove();
	//alert(closeEvents);
	$.each(closeEvents, function(index, closeEvent){
		retrieveMapLocation(closeEvent.eventlat, closeEvent.eventlon);
		$('#eventDetailsUsername').text(closeEvent.username);
		$('#eventDetailsName').text(closeEvent.eventname);
		$('#eventDetailsDate').text(eventDateFormat(closeEvent.eventdate));
		$('#eventDetailsAddress').text(closeEvent.eventstreet + ' , ' + closeEvent.eventcity + ' , ' + closeEvent.eventstate + ' ' + closeEvent.eventzip);
		$('#eventDetailsDistance').text(Math.ceil(closeEvent.eventdistance) + ' mi');
		$('#eventDetailsDescription').text('Description: ' + closeEvent.eventdescription);
	});	
}

function retrieveMapLocation(lat,lng) {
	var initialLocation = new google.maps.LatLng(lat, lng);
    var myOptions = {
        zoom: 15, 
        center: initialLocation, 
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: false,
        zoomControl: false,
        scaleControl: false
    };
    var map = new google.maps.Map(document.getElementById("mapEventCard"), myOptions);
    new google.maps.Marker ({ 
		map : map, 
		animation : google.maps.Animation.DROP,
		position : initialLocation 
	}); 
}
